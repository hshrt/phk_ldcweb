var serverIp = "202.181.241.56";
var test_phk_serverIp = "210.176.163.123"
var serverPath = "/"+local+"/api/laundry";
var logout_serverPath = "/"+local+"/api";
var audioFile = "media/Ding-small-bell.mp3";
var failAudio = "media/fail.mp3";
var stopUpdating = false;
var isPlacingOrder = false;

var Order = Backbone.Model.extend({
	getRequirement : function () {
		var requirementText = '';

		if (this.get('requirement') != undefined) {
			requirementText = this.get('requirement');
		}
		return requirementText;
	},

	getStatus : function () {
		return this.get('status');
	},
	getId : function () {
		return this.get('id');
	},
	getRoom : function () {
		return this.get('room');
	},
	getDeliveryDate : function () {
		return "";
	},
	getDeliveryTime : function () {
		return "";
	},
	getOrderDate : function () {
		return this.get('orderTime').split(" ")[0];
	},
	getOrderTime : function () {
		return this.get('orderTime').split(" ")[1];
	},
	getNumOfGuest : function () {
		return this.get('numOfGuest');
	},
	getQuantity : function () {
		return this.get('quantity');
	},
	getItem : function () {
		var item = "";
		var obj = JSON.parse(this.get('laundryIdList'));
		for (var i = 0; i < obj.length; i++) {
			if (item.length > 0) {
				item += "\n";
			}
			if (obj[i].parent && obj[i].grandparent) {
				item += obj[i].grandparent + "\n    > " + obj[i].item + " [" + obj[i].quantity + "]\n";
			} else {
				item += obj[i].item + " [" + obj[i].quantity + "]\n";
			}
			if (obj[i].choice) {
				var list = obj[i].choice.split(',');
				if (list.length > 0) {
					for (var j = 0; j < list.length; j++) {
						item += "\t- " + list[j].trim() + "\n";
					}
				}
			}
			if (obj[i].serviceType != 0) {
				item += "\t- Express\n";
			}

		}
		return item;
	},
	getChoices : function () {
		var temp = this.get('choices');
		if (!temp) return temp;
		else return temp.split(", ").join("\n");
	},
	getStatusTxt : function () {
		var status = false;
		status = this.get('status');
		switch (status){
			case '0':
				return "Waiting for process";
				break;
			case '1':
				return "Processed Manually";
				break;
			case '2':
				return "Cancelled";
				break;
			case '3':
				return "Failed";
				break;
			case '4':
				return "Completed";
				break;
		}
	},
	getColor: function() {
		var status = false;
		status = this.get('status');
		switch(status) {
			case '3':
				return 'red';
				break;
			default:
				return 'black';
				break;
		}
	},
	getDisplayOrderNowBtn: function() {
		var status = false;
		status = this.get('status');
		switch (status) {
			case '0':
				return "inline";
				break;
			default:
				return "none";
				break;
		}
	},
	getDisplayPendOrderBtn : function (){
		var status = false;
		status = this.get('status');
		switch (status){
			case '0':
				return "inline";
				break;
			case '1':
				return "none";
				break;
			case '2':
				return "none";
				break;
			case '3':
				return "inline";
				break;
			case '4':
				return "none";
				break;
		}
	},
	getDisplayCanOrderBtn : function(){
		var status = false;
		status = this.get('status');
		switch (status){
			case '0':
				return "inline";
				break;
			case '1':
				return "none";
				break;
			case '2':
				return "none";
				break;
			case '3':
				return "inline";
				break;
			case "4":
				return "none";
				break;

		}
	},
	getLastUpdateTime: function(){
		return this.get('lastUpdate');
	},
	getLastUpdateBy: function (){
		return this.get('lastUpdateBy');
	},
	getTxtColor: function(){
		var status = false;
		status = this.get('status');
		switch (status){
			case '0':
				return "color:red";
				break;
			case '1':
				return "color:green";
				break;
			case '2':
				return "color:black";
				break;
		}
	},
	getPoOn: function(){
		var status = false;
		status = this.get('status');
		switch (status){
			case '0':
				return "poOn";
				break;
			case '1':
				return "";
				break;
			case '2':
				return "";
				break;
			case '3':
				return "poFailOn";
				break;
		}
	},
	getScheduleTime: function(){
		return this.get("scheduleTime");
	}
});

var Orders = Backbone.Collection.extend({

	model : Order,
	searchDate : false,
	searchStatus : false,
	searchRoom : false,
	
	url : function () {
		console.log("url");
		//-- test --//
		//return 'http://'+test_phk_serverIp+serverPath+'/getOrder.php?' + 'date=' + encodeURIComponent(this.searchDate) + '&status=' + encodeURIComponent(this.searchStatus) + '&room=' + encodeURIComponent(this.searchRoom);
		
		return '..'+serverPath+'/getOrder.php?' + 'date=' + encodeURIComponent(this.searchDate) + '&status=' + encodeURIComponent(this.searchStatus) + '&room=' + encodeURIComponent(this.searchRoom);
	},
	parse : function (response) {
		console.log("parse: "+response.data);
		setUpdateTime();
		return response.data;
	},
	searchFor : function (date, status, room) {
		console.log("searchFor: "+'date=' + encodeURIComponent(date) + '&status=' + encodeURIComponent(status) + '&room=' + encodeURIComponent(room));
		if (date==null || date == 0 || !checkDateString(date)) date = getTodayDate();
		if (status==null) status="";
		if (room==null || room == 0) room="";
		this.searchDate = date;
		this.searchStatus = status;
		this.searchRoom = room;
		this.fetch();
		return this;
	}
});

var OrdersList = Backbone.View.extend({
	tagName: 'ul',
	id: 'results',
	className: 'list-group',
	template: function () { return ''; },
	initialize: function(options) {
		console.log("OrdersList init");
		this.listenTo(this.collection, 'sync', this.render);
		if (options.template) {
			this.template = options.template;
		}
	},
	render: function () {
		console.log("OrdersList render ");
		var templateData = {
			results: this.collection.map(this._generateRowData)
		};
		var html = this.template(templateData);
		this.$el.html(html);
		$(window).scrollReadPos();

		$(".poFailOn").effect("shake");
		$(".poOn").effect("shake");
		if ($(".poFailOn").length > 0) {
			playSound(failAudio);
		}
		else if ($(".poOn").length>0){
			playSound(audioFile);
		}
		
		return this;
	},
	_generateRowData: function (model) {
		console.log("The requirement is " + model.getRequirement());
		return {
			id:           		model.getId(),
			status:       		model.getStatus(),
			room:         		model.getRoom(),
			deliveryDate: 		model.getDeliveryDate(),
			deliveryTime: 		model.getDeliveryTime(),
			orderDate:    		model.getOrderDate(),
			orderTime:    		model.getOrderTime(),
			numOfGuest:   		model.getNumOfGuest(),
			quantity:     		model.getQuantity(),
			item:         		model.getItem(),
			choices:      		model.getChoices(),
			statusTxt:	  		model.getStatusTxt(),
			pendingBtnDisplay:  model.getDisplayPendOrderBtn(),
			cancelBtnDisplay: 	model.getDisplayCanOrderBtn(),
			lastUpdateTime:     model.getLastUpdateTime(),
			lastUpdateBy:       model.getLastUpdateBy(),
			txtColor:           model.getTxtColor(),
			poOn:               model.getPoOn(),
			requirement:        model.getRequirement(),
			orderBtnDisplay:    model.getDisplayOrderNowBtn(),
			scheduleTime:       model.getScheduleTime(),
			color: 				model.getColor()
		};
	},
	events: {
		'click .pending-order': 'pendingOrder',
		'click .update-button' : 'updateButtonClicked',
		'click .cancel': 'cancel'
	},
	pendingOrder: function(ev) {
		var id = $(ev.target).parents('li').attr('data-id');
		var status = $(ev.target).parents('li').attr('data-status');
		console.log("id: "+id+" s: "+status);
		$(ev.target).hide();
		switch(status){
			case "0":
				this.$('.process-order-'+id).show();
				this.$('.cancel-order-'+id).show();
				this.$('.unprocess-order-'+id).hide();
				break;
			case "1":
				this.$('.process-order-'+id).hide();
				this.$('.cancel-order-'+id).show();
				break;
			case "2":
				break;
			default:
		}
	},
	cancel: function() {
		resultView.render();
	}
	
});

var SearchForm = Backbone.View.extend({
	initialize: function () {
		if (this.collection) {
			this.listenTo(this.collection, 'sync', this.render);
		}
		console.log("order date: "+this.collection.searchDate);
		
		this.$("#order_date").change(function(){$("#order_date").trigger('submit')});
		this.$("#status").change(function(){$("#order_date").trigger('submit')});
		this.$("#room").change(function(){$("#order_date").trigger('submit')});
	},
	render: function () {
		this.$("#order_date").val(this.collection.searchDate);
		this.$("#status").val(this.collection.searchStatus);
		this.$("#room").val(this.collection.searchRoom);
	},
	events: {
		'submit': function (ev) {
			ev.preventDefault();
			var date = this.$("#order_date").val();
			var status = this.$("#status").val();
			if (status=="-1")status="";
			var room = this.$("#room").val();
			
			console.log("submit");
			console.log(date+","+status+","+room);
			
			this.trigger('search', date, status, room);
		}
	}
});

var PageRouter = Backbone.Router.extend({
	routes: {
		'search/(:date)+(:status)+(:room)':  'search'
	}
});

var searchResults = new Orders();

var resultView = new OrdersList({
	el: '#results',
	collection: searchResults,
	template: Handlebars.compile($('#order-list-template').html())
});


var router = new PageRouter();


var searchView = new SearchForm({
	el: '#search',
	collection: searchResults
});

var newDate=null, newStatus="0", newRoom=null;

router.on('route:search', function (date, status, room) {
	console.log("router.");
	searchResults.searchFor(date, status, room);
	newDate=date;
	newStatus=status;
	newRoom=room;
});

searchView.on('search', function (date, status, room) {
	console.log("searchView");
    if (!checkDateString(date)) date = date.substring(0, 10);
	router.navigate("search/"+encodeURIComponent(date)+"+"+encodeURIComponent(status)+"+"+encodeURIComponent(room), {trigger:true});
});
	
	Backbone.history.start();
	
$.fn.scrollSetPos = function(){
	console.log("scrollSetPos");
    if (localStorage) {
        var posReader = localStorage["posStorage"];
        $(window).scroll(function(e) {
            localStorage["posStorage"] = $(window).scrollTop();
			console.log("scrollSetPos: "+ localStorage["posStorage"]);
        });

        return true;
    }

    return false;
};

$.fn.scrollReadPos = function(){
	console.log("scrollReadPos");
    if (localStorage) {
        var posReader = localStorage["posStorage"];
        if (posReader) {
			console.log("scrollReadPos: "+posReader);
            $(window).scrollTop(posReader);
            localStorage.removeItem("posStorage");
        }
        return true;
    }

    return false;
};
    

$( document ).ready( function() {
	$(window).scrollSetPos();
	window.setInterval(function(){
			if (!stopUpdating) {
			    checkMintues();
                console.log("refresh");
			    searchResults.searchFor(newDate, newStatus, newRoom);
			}
		 }, 10000);
	searchResults.searchFor(newDate, newStatus, newRoom);
});

function checkMintues(){
    console.log("mintues");
    var date = new Date();
    var min = date.getMinutes();
    var sec = date.getSeconds();
    console.log(min);
    var hash = window.location.hash;
    console.log(hash);
    
    if (min == 0 && sec<10){
        console.log("reload");
        if (hash){
            window.location.hash = '';
        }
        window.location.reload();
    }
}

function SubForm (id, status){
	console.log(id+"+"+status);
    $.ajax({
		//--test link--
        //url:'http://'+test_phk_serverIp+serverPath+'/updateOrder.php?orderId='+id+'&status='+status,
		url:'..'+serverPath+'/updateOrder.php?orderId='+id+'&status='+status,
        type:'post',
        success:function(){
            if (status == '2') {
                alert("Order Cancelled!");
            } else {
                alert("Order Processed!");
            }
			window.location.reload();
        },
		error: function(){
			alert("Server is busy at the momnet, please try again later.")
			window.location.reload();
		}
    });
};

function placeOrderNow (id) {
	if (isPlacingOrder) {
		return;
	}

	isPlacingOrder = true;
	console.log(id);
	showLoading();
	$.ajax({
		url:'..'+serverPath+'/submitInstantOrder.php',
        type:'post',
        data: {
        	orderId: id
        },
        success:function(result){
        	isPlacingOrder = false;
        	hideLoading();
            alert(result);
			window.location.reload();
        },
		error: function(){
			isPlacingOrder = false;
			alert("Server is busy at the momnet, please try again later.")
			window.location.reload();
		}
    });
}

function showLoading(){
    console.log("showLoading!");
    $("#loadContainer").spin({color: '#FFF'});
    $("#loadContainer").css({'background-color': 'rgba(0,0,0,.3)'});
    $("#loadContainer").show();
}

function hideLoading(){
    console.log("hideLoading!");
    $("#loadContainer").spin(false);
    $("#loadContainer").css({'background-color': 'rgba(0,0,0,.0)'});
    $("#loadContainer").hide();
}

function editOrder (id, time) {
	stopUpdating = true;
	var guestSpan = $('#printarea-' + id).find('.numOfGuest');
	var guestEditText = $('#printarea-' + id).find('.guest_edit');
	var timeSelect = $('#printarea-' + id).find('#timeSelect');
	var editButton = $('#action-' + id).find('.edit');
	var cancelButton = $('#action-' + id).find('.cancel');
	var printButton = $('#action-' + id).find('.print-order');
	var processButton = $('#action-' + id).find('.pending-order');
	var orderNowButton = $('#action-' + id).find('.order-now');

	if (editButton.text() == "Edit") {
		guestSpan.hide();
		guestEditText.show();
		timeSelect.show();
		timeSelect.val(time);
		editButton.text("Save");
		cancelButton.hide();
		printButton.hide();
		processButton.hide();
		orderNowButton.hide();
	} else {
		console.log("need to save the data and refresh page");
		var numOfGuest = guestEditText.val();
		var scheduleTime = timeSelect.val();
		$.ajax({
			url:'..'+serverPath+'/updateOrder.php',
	        type:'post',
	        data: {
	        	orderId: id,
	        	numOfGuest: numOfGuest,
	        	scheduleTime: scheduleTime
	        },
	        success:function(result){
	            alert(result);
				window.location.reload();
	        },
			error: function(){
				alert("Server is busy at the momnet, please try again later.")
				window.location.reload();
			}
    	});
	}
}

function getTodayDate(){
	console.log("todayDate");
	var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} today = yyyy+'-'+mm+'-'+dd;
		return today;
}

function checkDateString(dateString){
    var regex = new RegExp("[0-9]{4}-[0-9]{2}-[0-9]{2}$");
    return regex.test(dateString);
}

function sessionEmail(){
	var msg = "Welcome, "+this.getSessionEmail();
	document.getElementById('topMsg').innerHTML=msg;
}

function todayDate(){
	document.getElementById('order_date').value=this.getTodayDate();
};

function setUpdateTime() {
	document.getElementById('update_time').innerHTML="As of " + this.getCurrentTime();
};

function getCurrentTime() {
	var dt = new Date();
	var minute = dt.getMinutes();
	var second = dt.getSeconds();
	var minuteFormatted = minute < 10 ? "0" + minute : minute;
	var secondFormatted = second < 10 ? "0" + second : second;
	var t = dt.getHours() + ":" + minuteFormatted + ":" + secondFormatted;
	return t;
};

function checkLogin(){
	console.log("checkLogin");
	if (sessionStorage.getItem('status') != 'loggedIn'){
    //redirect to page
		window.location.replace("login.html");
	}
	else{
    //show validation message
		return false;
	}
};

function checkIsHome() {
	var isHome = location.href.indexOf("#") == -1;
	if (!isHome) {
		$(".return-home").show();
	} else {
		$(".return-home").hide();
	}
};

function getSessionEmail(){
	return sessionStorage.getItem('email');
};

function logout(){
		$.ajax({
			url:'..'+logout_serverPath+'/logout.php',
			type:'post',
			success:function(){
				window.location.replace("login.html");
				sessionStorage.removeItem('status');
				sessionStorage.removeItem('email');
				sessionStorage.removeItem('fail');
			},
			error: function(){
				window.location.reload();
			}
		});
	};
	
function playSound(file){
	console.log("playSound");
	new Audio(file).play();
    
};

function checkUserAgent() {
    var x = navigator.userAgent;
    console.log(x);
    return x.includes("Android 3");
};

/*--This JavaScript method for Print Preview command--*/
function PrintPreview(id) {
    var toPrint = document.getElementById('printarea-'+id);
    var popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write('<html><title>::Print Preview::</title><link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css"><link rel="stylesheet" type="text/css" href="css/print.css" media="screen"/></head><body><div class="container"><h1>Laundry Order</h1>')
    popupWin.document.write(toPrint.innerHTML);
    popupWin.document.write('<button class="btn btn-info" onclick="myFunction()">Print</button><script>function myFunction() {window.print();}</script></html>');
    popupWin.document.close();
};

$(window).bind('hashchange', function() {
	checkIsHome();
});
